import { BreakPointAnalyticPage } from './app.po';

describe('break-point-analytic-web App', () => {
  let page: BreakPointAnalyticPage;

  beforeEach(() => {
    page = new BreakPointAnalyticPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
