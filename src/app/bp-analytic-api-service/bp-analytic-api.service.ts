import {Injectable} from '@angular/core';
import {Http, Response, URLSearchParams} from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/toPromise.js';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import {Observable} from 'rxjs';
import * as moment from 'moment/moment';


@Injectable()
export class BpAnalyticApiService {


  public static ApiAis = {
    Ip: '52.199.198.25',
    Port: '9001',
    Domain: 'lohas-uiux-ais'
  };

  public static ApiAnalysis = {
    Ip: '220.135.250.147',
    Port: '28080',
    Domain: 'lohas-uiux-analysis'
  };


  public static ApiAisRouteMapping = {
    getPatternScores: '/api/patternAnalyticsResult/search',
    getPatternsByProjectId_CaseId: '/api/pattern/search',
    getClusterRecordByProjectId_CaseId: '/api/weakSpotAnalytics/clusterProcessRecord',
    getWeakSpotByProject_CaseId: '/api/weakSpotAnalytics/weakSpot',
    createNewPatternReason: "/api/patternReason",
    getProjectLongTimeArray: '/api/weakSpotAnalytics/infos',
    weakSpotSequences: '/api/weakSpotAnalytics/weakSpotSequences',
    correctiveSuggestions: '/api/weakSpotAnalytics/correctiveSuggestions',
    deletePattern:'/api/pattern'

  };

  // DELETE http://52.199.198.25:9001/lohas-uiux-ais/api/pattern/{patternId}




  public static ApiAnalysisRouteMapping = {
    createNewPatternReason: '/ClusterChart2/createPatternReason',
    logFileUpload: "/File/doUploadLogFile",
    excelFileUpload: "/File/doUploadExcelFile",
    ca: "/Cross/CA",
    startFunction: "/Cross/startFunction",
    groupAnalysis: "/Cross/groupAnalysis"

  };


  constructor(private http: Http) {
  }

  uploadFile(url, formData) {

    // return this.http.post(url, formData, requestOptionArgs
    // ).map(this.extractDataBase)
    //   .catch(this.handleErrorPromise)

    return this.http.post(url, formData)
      .map(this.extractDataBase)
      .catch(this.handleErrorPromise)


  }


  getHttpRequest(ApiInfo, RouteMappingInfo, Filter) {
    let url = 'http://' + ApiInfo.Ip +
      ':' + ApiInfo.Port +
      '/' + ApiInfo.Domain + RouteMappingInfo;

    let params = new URLSearchParams();

    let getParamKeys = Object.keys(Filter);
    for (let i = 0; i < getParamKeys.length; i++) {
      params.set(getParamKeys[i], Filter[getParamKeys[i]]);
    }

    return this.http
      .get(url, {'search': params})
      .map(this.extractDataBase)
      .catch(this.handleErrorPromise);


  }

  postHttpRequest(ApiInfo, RouteMappingInfo, Filter) {
    let url = 'http://' + ApiInfo.Ip +
      ':' + ApiInfo.Port +
      '/' + ApiInfo.Domain + RouteMappingInfo;

    console.log(url);

    return this.http
      .post(url, Filter)
      .map(this.extractDataBase)
      .catch(this.handleErrorPromise);


  }


  postHttpRequestAis(ApiInfo, RouteMappingInfo, Filter) {
    let url = 'http://' + ApiInfo.Ip +
      ':' + ApiInfo.Port +
      '/' + ApiInfo.Domain + RouteMappingInfo;

    url += "?";
    let getParamKeys = Object.keys(Filter);
    for (let i = 0; i < getParamKeys.length; i++) {
      url += getParamKeys[i] + "=" + Filter[getParamKeys[i]] + "&";
    }
    url = url.substr(0, url.length - 1);

    console.log(url);


    let formData = new FormData();

    // for (let i = 0; i < getParamKeys.length; i++) {
    //   formData.append(getParamKeys[i], Filter[getParamKeys[i]]);
    // }

    return this.http
      .post(url, formData)
      .map(this.extractDataBase)
      .catch(this.handleErrorPromise);


  }


  putHttpRequest(ApiInfo, RouteMappingInfo, id, Filter) {
    let url = 'http://' + ApiInfo.Ip +
      ':' + ApiInfo.Port +
      '/' + ApiInfo.Domain + RouteMappingInfo + '/' + id;

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    return this.http
      .put(url, Filter, headers)
      .map(this.extractDataBase)
      .catch(this.handleErrorPromise);


  }


  deleteHttpRequest(ApiInfo, RouteMappingInfo, Filter) {
    let url = 'http://' + ApiInfo.Ip +
      ':' + ApiInfo.Port +
      '/' + ApiInfo.Domain + RouteMappingInfo + "/" + Filter.id;

    return this.http
      .delete(url)
      .map(this.extractDataDelete)
      .catch(this.handleErrorPromise);


  }


  private extractDataBase(res: Response): any {
    let resObj = res.json();
    return resObj || [];
  }


  private extractDataDelete(res: Response): any {
    return "ok";
  }


  private handleErrorPromise(error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }

}
