import { TestBed, inject } from '@angular/core/testing';

import { BpAnalyticApiService } from './bp-analytic-api.service';

describe('BpAnalyticApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BpAnalyticApiService]
    });
  });

  it('should be created', inject([BpAnalyticApiService], (service: BpAnalyticApiService) => {
    expect(service).toBeTruthy();
  }));
});
