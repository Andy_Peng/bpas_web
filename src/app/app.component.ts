import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserAuthService} from './user-auth-service/user-auth.service'
import {projectUploadTemplate} from "./vendor/OptionInterface/projectUploadTemplate";
import {caseUploadTemplate} from "./vendor/OptionInterface/caseUploadTemplate";
import {Tools} from './vendor/Tools/Tools';
import * as _ from 'lodash';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserAuthService]
})
export class AppComponent implements OnInit {
  _opened: boolean = false;
  _closeOnClickOutside: boolean = true;
  _animate: boolean = true;
  views: any[] = [];
  viewsBasic: any[] = [];


  userLogged: boolean = false;
  userId: string = "";
  passWd: string = "";

  public static projects: projectUploadTemplate[] = [];
  public static selectedCase: caseUploadTemplate;


  welcomeStr: string = "請登入";


  constructor(private router: Router, private userAuthService: UserAuthService) {


    this.viewsBasic = [
      {
        label: '斷點Pattern管裡',
        icon: 'fa-braille',
        urlStr: 'BreakPointPatternManagement',
        command: this.clickMenu
      },
      {
        label: '服務體驗斷點分析',
        icon: 'fa-line-chart',
        urlStr: '/ServiceExperienceBreakPointAnalytic',
        command: this.clickMenu
      },
      {
        label: '服務體驗斷點分析結果查詢',
        icon: 'fa-search',
        urlStr: '/ServiceExperienceBreakPointSearch',
        command: this.clickMenu
      },
      {
        label: '服務體驗流程修正與建議',
        icon: 'fa-bullseye',
        urlStr: '/ServiceExperienceBreakPointRecommand',
        command: this.clickMenu
      }

    ];


  }

  ngOnInit(): void {
    this.views = [{label: '請登入以開啟功能'}];
    this.welcomeStr = '請登入';

    this.userId = "itri65";
    this.passWd = "itri-itri65";


    setTimeout(() => {

    }, 500)


  }

  clickMenu = (event: any) => {
    if (event.item.urlStr != null) {
      this.router.navigateByUrl(event.item.urlStr);
      this._opened = false;
    }
  };

  _toggleSidebar() {
    this._opened = !this._opened;
  }


  login() {


    this.passWd = Tools.MD5(this.passWd);

    console.log(this.passWd);

    let Filter = {
      account: this.userId,
      password: this.passWd
    };




    this.userAuthService.login(
      UserAuthService.ApiAnalysis,
      UserAuthService.ApiAnalysisRouteMapping['login']
      , Filter
    ).subscribe((response) => {
      console.log(response);

      if (response.status == 500) {
        this.welcomeStr = "無此帳號";

      } else if (response.functionStatus === "SUCCESS") {

        this.userAuthService.postHttpRequest(UserAuthService.ApiAnalysis,
          UserAuthService.ApiAnalysisRouteMapping.getUserProject,
          {userId: this.userId}
        ).subscribe((response) => {
          console.log("response of getUserProject");
          console.log(response);


          if (response['accountFolder'].length > 0) {
            this.getAllCasesFromUser(response);
            this.userLogged = true;
            this.views = this.viewsBasic;
            //this.router.navigateByUrl("/ServiceExperienceBreakPointRecommand");
            this.router.navigateByUrl("/ServiceExperienceBreakPointAnalytic");
            //this.router.navigateByUrl("/BreakPointPatternManagement");
          } else {
            this.welcomeStr = "本帳號無專案，請新增";
          }


        })
      } else {
        this.welcomeStr = "密碼錯誤";
      }
    })
  }

  logOut(): void {
    this.userId = '';
    this.passWd = '';
    this.userLogged = false;
    //this.userToken = '';
    //this.userProps = [];
    this.views = [{label: '請登入以開啟功能'}];
    this.welcomeStr = '請登入';
    this.router.navigateByUrl("/BreakPointPatternManagement");
    //this.inPage = "";
  }

  getAllCasesFromUser(response) {

    AppComponent.projects = [];

    let data = response;

    for (let i = 0; i < data.accountFolder.length; i++) {
      let tmpIndex = _.findIndex(AppComponent.projects, {projectName: data.accountFolder[i][0]});

      if (tmpIndex == -1) {
        AppComponent.projects.push({
          projectName: data.accountFolder[i][0],
          cases: [{
            projectName: data.accountFolder[i][0],
            caseName: data.accountFolder[i][1],
            txtLogFile: {},
            excelFile: {},
            hidden: false,
          }]
        })
      } else {
        AppComponent.projects[tmpIndex].cases.push({
          projectName: data.accountFolder[i][0],
          caseName: data.accountFolder[i][1],
          txtLogFile: {},
          excelFile: {},
          hidden: false,
        })
      }


    }// enf of for_Scan accountFolder
    //this.changeToCase(0, 0);
  }


}

