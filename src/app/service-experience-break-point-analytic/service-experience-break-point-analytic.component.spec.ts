import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceExperienceBreakPointAnalyticComponent } from './service-experience-break-point-analytic.component';

describe('ServiceExperienceBreakPointAnalyticComponent', () => {
  let component: ServiceExperienceBreakPointAnalyticComponent;
  let fixture: ComponentFixture<ServiceExperienceBreakPointAnalyticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceExperienceBreakPointAnalyticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceExperienceBreakPointAnalyticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
