import {Component, OnInit, ViewChild} from '@angular/core';
import {IWorkBook} from "ts-xlsx";
import {IWorkSheet, IWritingOptions} from "xlsx";
import * as XLSX from 'ts-xlsx';


import {projectUploadTemplate} from "../vendor/OptionInterface/projectUploadTemplate";
import {caseUploadTemplate} from "../vendor/OptionInterface/caseUploadTemplate";
import {UserAuthService} from "../user-auth-service/user-auth.service";
import {BpAnalyticApiService} from '../bp-analytic-api-service/bp-analytic-api.service'
import {AppComponent} from "../app.component";
import {RequestOptions} from "@angular/http";
import {Observable} from 'rxjs/Rx';


// import * as XLSX from 'xlsx';

@Component({
  selector: 'app-service-experience-break-point-analytic',
  templateUrl: './service-experience-break-point-analytic.component.html',
  styleUrls: ['./service-experience-break-point-analytic.component.css'],
  providers: [UserAuthService, BpAnalyticApiService]
})
export class ServiceExperienceBreakPointAnalyticComponent implements OnInit {


  @ViewChild("fileUploadExcel") fileUploadExcel;
  @ViewChild("fileUploadTxt") fileUploadTxt;


  protected selectedCase: caseUploadTemplate;
  protected projects: projectUploadTemplate[] = [];

  protected AllFileNotReady: boolean = true;
  protected TxtFileNotReady: boolean = true;
  protected ExcelFileNotReady: boolean = true;

  protected selectedCaseFunctionListDataSets: any[];
  protected selectedCaseFunctionListDataSets_added: any[];

  protected selectedCaseFunctionListDataSets_processed: any[];
  protected selectedCaseWorkBook: IWorkBook;

  protected numOfCluster: string;

  private uploadExcel;

  protected loading: boolean = false;

  //protected messageStr: string = "";


  constructor(private userAuthService: UserAuthService, private bpAnalyticApiService: BpAnalyticApiService) {
  }

  ngOnInit() {
    this.projects = AppComponent.projects;
    // if (this.projects.length == 0) {
    //   this.messageStr = "本帳號無專案";
    //   console.log(this.messageStr);
    //
    // }

    this.changeToCase(0, 0);
    // this.projects.push({
    //   projectName: "lajsdkl",
    //   cases: []
    // });
    //
    // console.log(AppComponent.projects);
    //this.projects AppComponent.projects 連動沒問題
  }


  changeToCase(projectI, caseJ) {
    this.selectedCase = this.projects[projectI].cases[caseJ];
    //console.log(this.selectedCase);
    this.fileUploadTxt.clear();
    this.fileUploadExcel.clear();
    this.ExcelFileNotReady = true;
    this.TxtFileNotReady = true;
    this.AllFileNotReady = true;
    this.selectedCaseFunctionListDataSets = [];
  }


  removeExcel() {
    this.fileUploadExcel.clear();


  }

  uploadHistoryFile(event) {
    let formData = new FormData();
    //console.log(this.selectedCase.projectName);
    //console.log(this.selectedCase.caseName);

    formData.append("projectUuid", this.selectedCase.projectName);
    formData.append("Folder", this.selectedCase.caseName);
    formData.append("filelog", event['files'][0]);


    // let postData = {
    //   projectUuid: 'project03',
    //   Folder: 'case01'
    // };

    //let headers = new Headers({});
    //headers.set("body",postData);
    //let options = new RequestOptions({ headers });


    let url = "http://" + BpAnalyticApiService.ApiAnalysis.Ip
      + ":" + BpAnalyticApiService.ApiAnalysis.Port + "/"
      + BpAnalyticApiService.ApiAnalysis.Domain + BpAnalyticApiService.ApiAnalysisRouteMapping.logFileUpload;

    this.loading = true;

    this.bpAnalyticApiService.uploadFile(
      url,
      formData
    ).subscribe((response) => {
      console.log(response);
      this.TxtFileNotReady = false;
      this.loading = false;

      if (!this.ExcelFileNotReady && !this.TxtFileNotReady) {
        this.AllFileNotReady = false;
      }
    });
  }


  parseFunctionListExcel(event) {
    let reader = new FileReader();
    this.uploadExcel = event.files[0];
    reader.readAsBinaryString(event.files[0]);
    reader.onload = (evt) => {
      //document.getElementById("fileContents").innerHTML = ;
      let data = evt.target['result'];

      //value, {type: 'binary'}

      let wb: IWorkBook = XLSX.read(data, {type: 'binary'});
      this.selectedCaseWorkBook = wb;


      let SheetNames = wb.SheetNames;
      let workSheets = wb.Sheets;
      let currentSheet: IWorkSheet = wb.Sheets[wb.SheetNames[0]];
      let json = XLSX.utils.sheet_to_json(currentSheet, {raw: true, header: 'A'});
      //utils.aoa_to_sheet()

      console.log(currentSheet);


      this.selectedCase.excelFile = data;
      this.selectedCaseFunctionListDataSets = json;
      //console.log(this.selectedCase.functionListDataSets)

      this.ExcelFileNotReady = false;
      this.AllFileNotReady = false;

      // if (!this.ExcelFileNotReady && !this.TxtFileNotReady) {
      //   this.AllFileNotReady = false;
      // }


    }
  }

  stringToArrayBuffer(s: string): ArrayBuffer {
    const buf: ArrayBuffer = new ArrayBuffer(s.length);
    const view: Uint8Array = new Uint8Array(buf);
    for (let i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
  }

  analyze() {

    // console.log(this.selectedCaseFunctionListDataSets);
    // let aoa_excel = [];
    // let objKeys = Object.keys(this.selectedCaseFunctionListDataSets[0]);
    //
    // let header = [];
    // //let AAscii = 'A'.charCodeAt(0);
    // //a.push(String.fromCharCode(i));
    //
    // for (let i = 0; i < objKeys.length; i++) {
    //   header.push(objKeys[i])
    // }
    //
    // //aoa_excel.push(header);
    // console.log(this.selectedCaseFunctionListDataSets_added);
    //
    // for (let i = 0; i < this.selectedCaseFunctionListDataSets_added.length; i++) {
    //   let tmp = [];
    //   for (let j = 0; j < objKeys.length; j++) {
    //     tmp.push(this.selectedCaseFunctionListDataSets_added[i][objKeys[j]]);
    //   }
    //   aoa_excel.push(tmp);
    // }
    //
    // console.log(aoa_excel);
    // let newSheet: IWorkSheet = XLSX.utils.aoa_to_sheet(aoa_excel);
    // this.selectedCaseWorkBook.Sheets[this.selectedCaseWorkBook.SheetNames[0]] = newSheet;
    // console.log("newSheet end");
    //
    // let wOpts: IWritingOptions = {bookType: 'xlsx', type: 'binary'};
    // let newWorkBook: string = XLSX.write(this.selectedCaseWorkBook, wOpts);
    // console.log("newWorkBook end");
    //
    // let uploadExcel = new Blob([this.stringToArrayBuffer(newWorkBook)]);
    // console.log("transform end");


    //console.log(this.selectedCase.projectName);
    //console.log(this.selectedCase.caseName);


    //http://52.199.198.25:9001/lohas-uiux-ais/api/weakSpotAnalytics/weakSpotSequences?projectName=project03&caseName=pj03-case01&recordTime=1508741932098&limit=0.1&threshold=Average
    //http://52.199.198.25:9001/lohas-uiux-ais/api/weakSpotAnalytics/weakSpotSequences?projectName=project03&caseName=pj03-case01&recordTime=1508741932098&limit=0.1&threshold=Average


    //uploadFile
    let formData = new FormData();
    formData.append("projectUuid", this.selectedCase.projectName);
    formData.append("Folder", this.selectedCase.caseName);
    formData.append("fileSynopsis", this.uploadExcel);

    let url = "http://" + BpAnalyticApiService.ApiAnalysis.Ip
      + ":" + BpAnalyticApiService.ApiAnalysis.Port + "/"
      + BpAnalyticApiService.ApiAnalysis.Domain + BpAnalyticApiService.ApiAnalysisRouteMapping.excelFileUpload;


    this.loading = true;
    this.bpAnalyticApiService.uploadFile(url, formData).subscribe((response) => {
      console.log(response);
      console.log(parseInt(this.numOfCluster));
      this.fileUploadTxt.clear();
      this.fileUploadExcel.clear();
      this.ExcelFileNotReady = true;
      this.TxtFileNotReady = true;
      this.AllFileNotReady = true;


      formData = new FormData();
      formData.append("projectUuid", this.selectedCase.projectName);
      formData.append("Folder", this.selectedCase.caseName);

      this.bpAnalyticApiService.postHttpRequest(BpAnalyticApiService.ApiAnalysis,
        BpAnalyticApiService.ApiAnalysisRouteMapping.ca,
        formData
      ).subscribe((response) => {
        console.log(response);
        this.bpAnalyticApiService.postHttpRequest(BpAnalyticApiService.ApiAnalysis,
          BpAnalyticApiService.ApiAnalysisRouteMapping.startFunction,
          formData
        ).subscribe((response) => {
          console.log(response);

          formData = new FormData();
          formData.append("projectUuid", this.selectedCase.projectName);
          formData.append("Folder", this.selectedCase.caseName);

          let postGroupNum = "";
          for (let i = 0; i < this.selectedCaseFunctionListDataSets_added.length; i++) {
            postGroupNum += this.selectedCaseFunctionListDataSets_added[i]['A'] + ",";
          }
          postGroupNum = postGroupNum.substr(0, postGroupNum.length - 1);

          console.log(postGroupNum);
          console.log(this.numOfCluster);

          formData.append("numberOfCluster", this.numOfCluster);
          formData.append("groupNum", postGroupNum);


          this.bpAnalyticApiService.postHttpRequest(BpAnalyticApiService.ApiAnalysis,
            BpAnalyticApiService.ApiAnalysisRouteMapping.groupAnalysis,
            formData
          ).subscribe((response) => {
            console.log(response);
            this.loading = false;
            let dateLong = parseInt(response['dateTime']);
            let filter = {
              projectName: this.selectedCase.projectName,
              caseName: this.selectedCase.caseName,
              recordTime: dateLong,
              limit: 0.1,
              threshold: 'Average'
            };

            this.bpAnalyticApiService.postHttpRequestAis(
              BpAnalyticApiService.ApiAis,
              BpAnalyticApiService.ApiAisRouteMapping.weakSpotSequences,
              filter).subscribe((response) => {

              let filter2 = {
                projectName: this.selectedCase.projectName,
                caseName: this.selectedCase.caseName,
                recordTime: dateLong,
                limit: 0.1,
                threshold: 'Average',
                rollWindowSize: 3
              };

              this.bpAnalyticApiService.postHttpRequestAis(BpAnalyticApiService.ApiAis,
                BpAnalyticApiService.ApiAisRouteMapping.correctiveSuggestions,
                filter2).subscribe((response) => {
                console.log(response);

              })
            })
          })
        })
      })
    })


  }

  private extractDataBase(res: Response): any {
    let resObj = res.json();
    return resObj || [];
  }

  private handleErrorPromise(error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }

}
