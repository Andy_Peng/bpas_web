import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http'
import {SidebarModule} from 'ng-sidebar';
import {AppRouteModule} from './app-route/app-route.module'
import {NgModule} from '@angular/core';

import {
  ButtonModule,
  CalendarModule,
  ChartModule,
  CodeHighlighterModule,
  ConfirmDialogModule,
  DataGridModule,
  DataTableModule,
  DialogModule,
  DropdownModule,
  FieldsetModule,
  FileUploadModule,
  InputTextModule,
  MenuModule,
  PanelMenuModule,
  PanelModule,
  PasswordModule,
  ScheduleModule,
  SharedModule,
  TabViewModule,
  ToolbarModule
} from 'primeng/primeng';


import {AppComponent} from './app.component';
import {BreakPointPatternManagementComponent} from './break-point-pattern-management/break-point-pattern-management.component';
import {ServiceExperienceBreakPointAnalyticComponent} from './service-experience-break-point-analytic/service-experience-break-point-analytic.component';
import {ServiceExperienceBreakPointRecommandComponent} from './service-experience-break-point-recommand/service-experience-break-point-recommand.component';
import {CaseFlowChartComponent} from './case-flow-chart/case-flow-chart.component';
import {BasicPatternFlowChartComponent} from './basic-pattern-flow-chart/basic-pattern-flow-chart.component';
import { ServiceExperienceBreakPointSearchComponent } from './service-experience-break-point-search/service-experience-break-point-search.component';


@NgModule({
  declarations: [
    AppComponent,
    BreakPointPatternManagementComponent,
    ServiceExperienceBreakPointAnalyticComponent,
    ServiceExperienceBreakPointRecommandComponent,
    CaseFlowChartComponent,
    BasicPatternFlowChartComponent,
    ServiceExperienceBreakPointSearchComponent
  ],
  imports: [
    AppRouteModule,
    BrowserModule,
    BrowserAnimationsModule,
    ButtonModule,
    CalendarModule,
    ChartModule,
    CodeHighlighterModule,
    ConfirmDialogModule,
    DataGridModule,
    DataTableModule,
    DialogModule,
    DropdownModule,
    FieldsetModule,
    FileUploadModule,
    FormsModule,
    HttpModule,
    InputTextModule,
    MenuModule,
    PanelMenuModule,
    PanelModule,
    PasswordModule,
    ScheduleModule,
    SharedModule,
    SidebarModule.forRoot(),
    TabViewModule,
    ToolbarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
