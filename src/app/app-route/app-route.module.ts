import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router'
import {CommonModule} from '@angular/common';
import {BreakPointPatternManagementComponent} from '../break-point-pattern-management/break-point-pattern-management.component'
import {ServiceExperienceBreakPointAnalyticComponent} from '../service-experience-break-point-analytic/service-experience-break-point-analytic.component';
import {ServiceExperienceBreakPointRecommandComponent} from '../service-experience-break-point-recommand/service-experience-break-point-recommand.component';
import {ServiceExperienceBreakPointSearchComponent} from '../service-experience-break-point-search/service-experience-break-point-search.component'


const routes: Routes = [

  {path: 'BreakPointPatternManagement', component: BreakPointPatternManagementComponent},
  {path: 'ServiceExperienceBreakPointAnalytic', component: ServiceExperienceBreakPointAnalyticComponent},
  {path: 'ServiceExperienceBreakPointRecommand', component: ServiceExperienceBreakPointRecommandComponent},
  {path: 'ServiceExperienceBreakPointSearch', component: ServiceExperienceBreakPointSearchComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRouteModule {
}
