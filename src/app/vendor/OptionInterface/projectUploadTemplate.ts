import {caseUploadTemplate} from "./caseUploadTemplate";

export interface projectUploadTemplate {
  projectName: string
  cases: caseUploadTemplate[]
}
