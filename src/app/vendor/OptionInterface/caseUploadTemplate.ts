export interface caseUploadTemplate {
  projectName: string,
  caseName: string,
  txtLogFile: any,
  excelFile: any,
  hidden: boolean
}
