/**
 * Created by andypeng on 2017/6/7.
 */
import {GoJsDataModel} from "../OptionInterface/GoJsDataModel"
import * as _ from 'lodash'
// import {until} from "selenium-webdriver";
// import stalenessOf = until.stalenessOf;


export class Tools {
  static formatDateYYYYMMDD = function (date: Date, SeparationSign) {
    let dformat = [date.getFullYear(),
      this.padLeft((date.getMonth() + 1), 10, '0'),
      this.padLeft(date.getDate(), 10, '0'),
    ].join(SeparationSign);
    return dformat;

  };

  static formatDateYmdHms = function (date: Date, SeparationSign) {
    let dformat = [
        date.getFullYear(),
        this.padLeft((date.getMonth() + 1), 10, '0'),
        this.padLeft(date.getDate(), 10, '0'),
      ].join(SeparationSign) + ' ' +
      [this.padLeft(date.getHours(), 10, '0'),
        this.padLeft(date.getMinutes(), 10, '0'),
        this.padLeft(date.getSeconds(), 10, '0')
      ].join(':');
    return dformat;
  };

  static formatDateMdHm = function (date: Date, SeparationSign) {
    let dformat = [
        this.padLeft((date.getMonth() + 1), 10, '0'),
        this.padLeft(date.getDate(), 10, '0'),
      ].join(SeparationSign) + ' ' +
      [this.padLeft(date.getHours(), 10, '0'),
        this.padLeft(date.getMinutes(), 10, '0')
      ].join(':');
    return dformat;
  };

  static formatDateYmdHmsMilli = function (date: Date, SeparationSign) {
    let dformat = [
        date.getFullYear(),
        this.padLeft((date.getMonth() + 1), 10, '0'),
        this.padLeft(date.getDate(), 10, '0'),
      ].join(SeparationSign) + ' ' +
      [this.padLeft(date.getHours(), 10, '0'),
        this.padLeft(date.getMinutes(), 10, '0'),
        this.padLeft(date.getSeconds(), 10, '0')
      ].join(':') + '.' + date.getMilliseconds();
    return dformat;
  };

  static padLeft = function (num: number, base, chr) {
    let len = (String(base || 10).length - String(num).length) + 1;
    return len > 0 ? new Array(len).join(chr || '0') + num : num;
  };

  static getLineChartData = function (data, targetColumns, detailColumns) {
    let lineChartData = {
      labels: [],
      datasets: []
    };

    let colors = ['#ff7f0e', '#2ca02c', '#d62728', '#9467bd'
      , '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'];


    let targetTag = 0;

    for (let i = 0; i < data.length; i++) {
      lineChartData.labels.push(data[i][targetColumns[targetTag]]);
    }
    targetTag++;


    for (let j = 1; j < targetColumns.length; j++) {
      let tmpDataSets = {
        label: targetColumns[targetTag],
        data: [],
        fill: false,
        borderColor: colors[targetTag - 1 % colors.length],
        pointBackgroundColor: colors[targetTag - 1 % colors.length],
        pointRadius: 5,
        pointHoverRadius: 8,
        lineTension: 0,
        detail: []
      };

      for (let i = 0; i < data.length; i++) {
        tmpDataSets.data.push(data[i][targetColumns[j]]);
        let tmpDetail = {};
        for (let t = 0; t < detailColumns.length; t++) {
          tmpDetail[detailColumns[t]] = data[i][detailColumns[t]];
        }
        tmpDataSets.detail.push(tmpDetail);
        //tmpDataSets.data.push("1");
      }
      lineChartData.datasets.push(tmpDataSets);
      targetTag++;
    }


    return lineChartData;
  }

  static getBarChartData = function (data, targetLabelColumn, targetDataColumns, detailColumns) {
    let barChartData = {
      labels: [],
      datasets: []
    };

    let colors = ['#ff7f0e', '#2ca02c', '#d62728', '#9467bd'
      , '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf'];


    for (let i = 0; i < data.length; i++) {

      if (typeof data[i][targetLabelColumn] === 'object') {
        barChartData.labels.push(Tools.formatDateMdHm(data[i][targetLabelColumn], '-'));
      } else {
        barChartData.labels.push(data[i][targetLabelColumn]);
      }


      for (let j = 0; j < targetDataColumns.length; j++) {
        if (barChartData.datasets[j] === undefined) {
          //console.log(barChartData.datasets[j]);
          //barChartData.datasets.push()''
          barChartData.datasets[j] = {
            label: targetDataColumns[j],
            data: [],
            //fill: false,
            borderColor: colors[j % colors.length],
            backgroundColor: colors[j % colors.length],
            detail: []
          }
        } else {
          //console.log(barChartData.datasets[j]);
          barChartData.datasets[j].data.push(data[i][targetDataColumns[j]]);
          let tmpDetail = {};
          for (let t = 0; t < detailColumns.length; t++) {
            tmpDetail[detailColumns[t]] = data[i][detailColumns[t]];
          }
          barChartData.datasets[j].detail.push(tmpDetail);
        }
      }
    }


    return barChartData;


  }

  static addDays = function (date, days) {
    let dat = new Date(date.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
  };

  static addMonths = function (date, months) {
    let dat = new Date(date.valueOf());
    dat.setMonth(dat.getMonth() + months);
    return dat;
  };

  //ChartJs Tooltip Callback function
  static chartJsCustomTooltipsLabel = function (tooltipItem, data) {
    let keys = Object.keys(data.datasets[tooltipItem.datasetIndex].detail[tooltipItem.index])
    let rlt = [];
    for (let i = 0; i < keys.length; i++) {
      rlt.push(keys[i] + " : " + data.datasets[tooltipItem.datasetIndex].detail[tooltipItem.index][keys[i]]);
    }
    return rlt;
  };

  static chartJsCustomTooltipsColor = function (tooltipItem, chart) {
    return {
      borderColor: 'rgba(0,0,0,0)',
    };
  };


  static getWeakSpotsArray(weakSpotRes) {
    let rlt = {};

    let names = ['exit', 'fail'];


    for (let nameIndex = 0; nameIndex < names.length; nameIndex++) {
      let ObjKeys = Object.keys(weakSpotRes[names[nameIndex]]);
      for (let i = 0; i < ObjKeys.length; i++) {
        if (rlt.hasOwnProperty(ObjKeys[i])) {
          for (let j = 0; j < weakSpotRes[names[nameIndex]][ObjKeys[i]].length; j++) {
            rlt[ObjKeys[i]].push(weakSpotRes[names[nameIndex]][ObjKeys[i]][j].id);
          }
        } else {
          rlt[ObjKeys[i]] = [];
          for (let j = 0; j < weakSpotRes[names[nameIndex]][ObjKeys[i]].length; j++) {
            rlt[ObjKeys[i]].push(weakSpotRes[names[nameIndex]][ObjKeys[i]][j].id);
          }
        }
      }
    }


    console.log(rlt);

    return rlt;


  }


  // static getWeakSpotsArrayDetail(){}

  static clusterProcessRecordDataToGoJsModel = function (clusterProcessRecordData, weakSpots) {

    let rlt: GoJsDataModel[] = [];
    //console.log(clusterProcessRecordData);

    for (let i = 0; i < clusterProcessRecordData.clusters.length; i++) {
      if (weakSpots.hasOwnProperty(i)) {
        rlt.push(Tools.ProcessCluster(clusterProcessRecordData, i, weakSpots[i]));
      } else {
        rlt.push(Tools.ProcessCluster(clusterProcessRecordData, i, []));
      }
      ;
    }

    return rlt;
  };


  static ProcessCluster(data, clusterIndex, weakSpotIndexes): GoJsDataModel {

    console.log("-------------------------------------------------");
    console.log("第" + clusterIndex + "群");


    //console.log(weakSpotIndexes);
    let rlt: GoJsDataModel = {
      nodesModel: [],
      linksModel: []
    };

    let locX: number = 0;
    let locY: number = 0;
    let MaxLocX: number = 0;
    let MaxLocY: number = 0;


    let locXInterval = 150;
    let locYInterval = 150;
    let locXStart = 50;
    let locYStart = 50;

    locX = locXStart;
    locY = locYStart;


    let percent = Math.round(data.clusters[clusterIndex].rate * 100);
    //console.log(data.clusters[clusterIndex].rate * 100);


    rlt.nodesModel.push(
      {
        key: (-2).toString(),
        color: "lightblue",
        name: "群" + (clusterIndex + 1) + "\n" +
        percent + "%",
        loc: locX + " " + locY,
        leftArray: [],
        rightArray: []
      });

    rlt.nodesModel.push(
      {
        key: (-1).toString(),
        color: "lightblue",
        name: "離開",
        loc: "",
        leftArray: [],
        rightArray: []
      });


    let usedFunctions = [];
    //Find the max value in functions
    //session,function mapping information is begin from index1
    //session[1]<------->function[0]
    let maxIndex = 1;
    let maxValue = parseFloat(data.clusters[clusterIndex].session[0][maxIndex]);

    let stack = [];
    let limit = 0.1;
    let tmpFloat = 0;

    for (let i = 2; i < data.clusters[clusterIndex].session[0].length; i++) {
      tmpFloat = parseFloat(data.clusters[clusterIndex].session[0][i]);
      if (tmpFloat > maxValue) {
        maxIndex = i;
        maxValue = tmpFloat;
      }
      if (tmpFloat > limit) {
        console.log(i + ":" + tmpFloat);
        stack.push(i);

        // rlt.nodesModel.push({
        //   key: (i - 1).toString(),
        //   color: "lightblue",
        //   index: data.functions[i - 1].funcId,
        //   name: data.functions[i - 1].funcName
        //   + "\nTime:" + data.clusters[clusterIndex].mean[i - 1]
        //   + "\nFail:" + data.clusters[clusterIndex].fail[i - 1],
        //   loc: locX + " " + locY,
        //   leftArray: [],
        //   rightArray: []
        // });

        // rlt.linksModel.push({
        //   from: (stack[i] - 1).toString(),
        //   to: (j - 1).toString(),
        //   rate: ((tmpFloat * 100).toFixed(0)).toString() + "%",
        //   fromPort: "outputTo" + (j - 1),
        //   toPort: "inputFrom" + (stack[i] - 1)
        // });


      }
    }

    let used = [];
    //let stack = [maxIndex];
    let pushedNode = [];


    while (stack.length > 0) {
      let stackLengthStageN = stack.length;

      let stacksNextStage = [];
      if (stackLengthStageN > 0) {
        locX += locXInterval;
      }
      locY = locYStart;


      console.log(data.clusters[clusterIndex]);

      //從第一排找到的長出去
      for (let i = 0; i < stackLengthStageN; i++) {
        for (let j = 0; j < data.clusters[clusterIndex].session[0].length; j++) {
          tmpFloat = parseFloat(data.clusters[clusterIndex].session[stack[i]][j]);
          if (tmpFloat > 0.1) {

            rlt.linksModel.push({
              from: (stack[i] - 1).toString(),
              to: (j - 1).toString(),
              rate: ((tmpFloat * 100).toFixed(0)).toString() + "%",
              fromPort: "outputTo" + (j - 1),
              toPort: "inputFrom" + (stack[i] - 1)
            });

            let nodeIndex = pushedNode.indexOf(stack[i]);

            if (stack[i] !== 0) {
              if (nodeIndex === -1) {
                pushedNode.push(stack[i]);
                rlt.nodesModel.push(
                  {
                    key: (stack[i] - 1).toString(),
                    color: "lightblue",
                    index: data.functions[stack[i] - 1].funcId,
                    name: data.functions[stack[i] - 1].funcName
                    + "\nTime:" + data.clusters[clusterIndex].mean[stack[i] - 1]
                    + "\nFail:" + data.clusters[clusterIndex].fail[stack[i] - 1],
                    loc: locX + " " + locY,
                    leftArray: [],
                    rightArray: [{"portId": "outputTo" + (j - 1), portColor: "orange"}]
                  });
                locY += locYInterval;
                MaxLocY = Math.max(locY, MaxLocY);
                MaxLocX = Math.max(locX, MaxLocX);
              } else {

                //本來就有兩個NODE 0:群N  1:離開 所以要+2
                rlt.nodesModel[nodeIndex + 2].rightArray.push(
                  {"portId": "outputTo" + (j - 1), portColor: "orange"}
                );


              }
            }//end of if(stack[i]!==0)

            nodeIndex = pushedNode.indexOf(j);

            if (j !== 0) {
              if (nodeIndex === -1) {
                pushedNode.push(j);
                rlt.nodesModel.push(
                  {
                    key: (j - 1).toString(),
                    color: "lightblue",
                    index: data.functions[j - 1].funcId,
                    name: data.functions[j - 1].funcName
                    + "\nTime:" + data.clusters[clusterIndex].mean[j - 1]
                    + "\nFail:" + data.clusters[clusterIndex].fail[j - 1],
                    loc: locX + " " + locY,
                    leftArray: [{"portId": "inputFrom" + (stack[i] - 1), portColor: 'green'}],
                    rightArray: []
                  });

                locY += locYInterval;
                MaxLocY = Math.max(locY, MaxLocY);
                MaxLocX = Math.max(locX, MaxLocX);
              } else {
                //本來就有兩個NODE 0:群N  1:離開 所以要+2
                rlt.nodesModel[nodeIndex + 2].leftArray.push(
                  {"portId": "inputFrom" + (stack[i] - 1), portColor: 'green'}
                );
              }
            }//end of if(j!==0)
            else {
              nodeIndex = 1;//index of Node_exit
              rlt.nodesModel[nodeIndex].leftArray.push(
                {"portId": "inputFrom" + (stack[i] - 1), portColor: 'green'}
              );

            }

            //add node to next search step
            if (
              used.indexOf(j) === -1 &&
              stacksNextStage.indexOf(j) === -1 &&
              stack.indexOf(j) === -1 &&
              j != 0
            ) {
              stacksNextStage.push(j);
            }
          }
        }
        used.push(stack[i]);
      }
      stack = stacksNextStage;
    }


    if (rlt.nodesModel.length == 2) {

      for (let i = 2; i < data.clusters[clusterIndex].session[0].length; i++) {
        tmpFloat = parseFloat(data.clusters[clusterIndex].session[0][i]);
        if (tmpFloat > limit) {
          rlt.nodesModel.push({
            key: (i - 1).toString(),
            color: "lightblue",
            index: data.functions[i - 1].funcId,
            name: data.functions[i - 1].funcName
            + "\nTime:" + data.clusters[clusterIndex].mean[i - 1]
            + "\nFail:" + data.clusters[clusterIndex].fail[i - 1],
            loc: locX + " " + locY,
            leftArray: [],
            rightArray: [{"portId": "outputTo" + rlt.nodesModel[1].key, portColor: "orange"}]
          });

          rlt.nodesModel[1].leftArray.push({"portId": "inputFrom" + (i - 1).toString(), portColor: "green"});

          MaxLocX = (locX + locXInterval * 1.3);

          rlt.linksModel.push({
            from: (i - 1).toString(),
            to: (-1).toString(),
            rate: (data.clusters[clusterIndex].session[i][0] * 100).toFixed(0).toString() + "%",
            fromPort: "outputTo" + rlt.nodesModel[1].key,
            toPort: "inputFrom" + (-2)
          });

        }
      }
    }


    for (let i = 1; i < data.clusters[clusterIndex].session[0].length; i++) {
      if (data.clusters[clusterIndex].session[0][i] > 0.1) {
        let rltIndex = _.findIndex(rlt.nodesModel, {index: data.functions[i - 1].funcId});
        if (rltIndex > 0) {

          rlt.nodesModel[0].rightArray.push(
            {"portId": "outputTo" + rlt.nodesModel[rltIndex].key, portColor: "orange"}
          );
          rlt.nodesModel[rltIndex].leftArray.push(
            {"portId": "inputFrom" + (-2).toString(), portColor: "green"}
          );
          rlt.linksModel.push({
            from: (-2).toString(),
            to: (i - 1).toString(),
            rate: (data.clusters[clusterIndex].session[0][i] * 100).toFixed(0).toString() + "%",
            fromPort: "outputTo" + rlt.nodesModel[rltIndex].key,
            toPort: "inputFrom" + (-2)
          });


        }

      }
    }


    // form node_clusterN_start  index:0   key:-2
    // to first Node In chart    index:2   key:rlt.nodesModel[2].key
    console.log("nodes");
    console.log(rlt);


    if (rlt.nodesModel[0].rightArray.length == 0) {
      rlt.nodesModel[0].rightArray.push(
        {"portId": "outputTo" + rlt.nodesModel[2].key, portColor: "orange"}
      );

      rlt.nodesModel[2].leftArray.push(
        {"portId": "inputFrom" + (-2).toString(), portColor: "green"}
      );

      rlt.linksModel.push({
        from: (-2).toString(),
        to: (maxIndex - 1).toString(),
        rate: (maxValue * 100).toFixed(0).toString() + "%",
        fromPort: "outputTo" + rlt.nodesModel[2].key,
        toPort: "inputFrom" + (-2)
      });
    }

    //console.log(pushedNode);


    rlt.nodesModel[1].loc = (MaxLocX + locXInterval) + " " + MaxLocY;

    for (let i = 0; i < rlt.nodesModel.length; i++) {
      if (rlt.nodesModel[i].hasOwnProperty('index')) {
        if (weakSpotIndexes.indexOf(parseInt(rlt.nodesModel[i].index)) >= 0) {
          rlt.nodesModel[i].color = "red";
        }
      }
    }

    console.log("-------------------------------------------------");
    return rlt;
  }

  static MD5 = function (s) {
    function L(k, d) {
      return (k << d) | (k >>> (32 - d))
    }

    function K(G, k) {
      var I, d, F, H, x;
      F = (G & 2147483648);
      H = (k & 2147483648);
      I = (G & 1073741824);
      d = (k & 1073741824);
      x = (G & 1073741823) + (k & 1073741823);
      if (I & d) {
        return (x ^ 2147483648 ^ F ^ H)
      }
      if (I | d) {
        if (x & 1073741824) {
          return (x ^ 3221225472 ^ F ^ H)
        } else {
          return (x ^ 1073741824 ^ F ^ H)
        }
      } else {
        return (x ^ F ^ H)
      }
    }

    function r(d, F, k) {
      return (d & F) | ((~d) & k)
    }

    function q(d, F, k) {
      return (d & k) | (F & (~k))
    }

    function p(d, F, k) {
      return (d ^ F ^ k)
    }

    function n(d, F, k) {
      return (F ^ (d | (~k)))
    }

    function u(G, F, aa, Z, k, H, I) {
      G = K(G, K(K(r(F, aa, Z), k), I));
      return K(L(G, H), F)
    }

    function f(G, F, aa, Z, k, H, I) {
      G = K(G, K(K(q(F, aa, Z), k), I));
      return K(L(G, H), F)
    }

    function D(G, F, aa, Z, k, H, I) {
      G = K(G, K(K(p(F, aa, Z), k), I));
      return K(L(G, H), F)
    }

    function t(G, F, aa, Z, k, H, I) {
      G = K(G, K(K(n(F, aa, Z), k), I));
      return K(L(G, H), F)
    }

    function e(G) {
      var Z;
      var F = G.length;
      var x = F + 8;
      var k = (x - (x % 64)) / 64;
      var I = (k + 1) * 16;
      var aa = Array(I - 1);
      var d = 0;
      var H = 0;
      while (H < F) {
        Z = (H - (H % 4)) / 4;
        d = (H % 4) * 8;
        aa[Z] = (aa[Z] | (G.charCodeAt(H) << d));
        H++
      }
      Z = (H - (H % 4)) / 4;
      d = (H % 4) * 8;
      aa[Z] = aa[Z] | (128 << d);
      aa[I - 2] = F << 3;
      aa[I - 1] = F >>> 29;
      return aa
    }

    function B(x) {
      var k = "", F = "", G, d;
      for (d = 0; d <= 3; d++) {
        G = (x >>> (d * 8)) & 255;
        F = "0" + G.toString(16);
        k = k + F.substr(F.length - 2, 2)
      }
      return k
    }

    function J(k) {
      k = k.replace(/rn/g, "n");
      var d = "";
      for (var F = 0; F < k.length; F++) {
        var x = k.charCodeAt(F);
        if (x < 128) {
          d += String.fromCharCode(x)
        } else {
          if ((x > 127) && (x < 2048)) {
            d += String.fromCharCode((x >> 6) | 192);
            d += String.fromCharCode((x & 63) | 128)
          } else {
            d += String.fromCharCode((x >> 12) | 224);
            d += String.fromCharCode(((x >> 6) & 63) | 128);
            d += String.fromCharCode((x & 63) | 128)
          }
        }
      }
      return d
    }

    var C = Array();
    var P, h, E, v, g, Y, X, W, V;
    var S = 7, Q = 12, N = 17, M = 22;
    var A = 5, z = 9, y = 14, w = 20;
    var o = 4, m = 11, l = 16, j = 23;
    var U = 6, T = 10, R = 15, O = 21;
    s = J(s);
    C = e(s);
    Y = 1732584193;
    X = 4023233417;
    W = 2562383102;
    V = 271733878;
    for (P = 0; P < C.length; P += 16) {
      h = Y;
      E = X;
      v = W;
      g = V;
      Y = u(Y, X, W, V, C[P + 0], S, 3614090360);
      V = u(V, Y, X, W, C[P + 1], Q, 3905402710);
      W = u(W, V, Y, X, C[P + 2], N, 606105819);
      X = u(X, W, V, Y, C[P + 3], M, 3250441966);
      Y = u(Y, X, W, V, C[P + 4], S, 4118548399);
      V = u(V, Y, X, W, C[P + 5], Q, 1200080426);
      W = u(W, V, Y, X, C[P + 6], N, 2821735955);
      X = u(X, W, V, Y, C[P + 7], M, 4249261313);
      Y = u(Y, X, W, V, C[P + 8], S, 1770035416);
      V = u(V, Y, X, W, C[P + 9], Q, 2336552879);
      W = u(W, V, Y, X, C[P + 10], N, 4294925233);
      X = u(X, W, V, Y, C[P + 11], M, 2304563134);
      Y = u(Y, X, W, V, C[P + 12], S, 1804603682);
      V = u(V, Y, X, W, C[P + 13], Q, 4254626195);
      W = u(W, V, Y, X, C[P + 14], N, 2792965006);
      X = u(X, W, V, Y, C[P + 15], M, 1236535329);
      Y = f(Y, X, W, V, C[P + 1], A, 4129170786);
      V = f(V, Y, X, W, C[P + 6], z, 3225465664);
      W = f(W, V, Y, X, C[P + 11], y, 643717713);
      X = f(X, W, V, Y, C[P + 0], w, 3921069994);
      Y = f(Y, X, W, V, C[P + 5], A, 3593408605);
      V = f(V, Y, X, W, C[P + 10], z, 38016083);
      W = f(W, V, Y, X, C[P + 15], y, 3634488961);
      X = f(X, W, V, Y, C[P + 4], w, 3889429448);
      Y = f(Y, X, W, V, C[P + 9], A, 568446438);
      V = f(V, Y, X, W, C[P + 14], z, 3275163606);
      W = f(W, V, Y, X, C[P + 3], y, 4107603335);
      X = f(X, W, V, Y, C[P + 8], w, 1163531501);
      Y = f(Y, X, W, V, C[P + 13], A, 2850285829);
      V = f(V, Y, X, W, C[P + 2], z, 4243563512);
      W = f(W, V, Y, X, C[P + 7], y, 1735328473);
      X = f(X, W, V, Y, C[P + 12], w, 2368359562);
      Y = D(Y, X, W, V, C[P + 5], o, 4294588738);
      V = D(V, Y, X, W, C[P + 8], m, 2272392833);
      W = D(W, V, Y, X, C[P + 11], l, 1839030562);
      X = D(X, W, V, Y, C[P + 14], j, 4259657740);
      Y = D(Y, X, W, V, C[P + 1], o, 2763975236);
      V = D(V, Y, X, W, C[P + 4], m, 1272893353);
      W = D(W, V, Y, X, C[P + 7], l, 4139469664);
      X = D(X, W, V, Y, C[P + 10], j, 3200236656);
      Y = D(Y, X, W, V, C[P + 13], o, 681279174);
      V = D(V, Y, X, W, C[P + 0], m, 3936430074);
      W = D(W, V, Y, X, C[P + 3], l, 3572445317);
      X = D(X, W, V, Y, C[P + 6], j, 76029189);
      Y = D(Y, X, W, V, C[P + 9], o, 3654602809);
      V = D(V, Y, X, W, C[P + 12], m, 3873151461);
      W = D(W, V, Y, X, C[P + 15], l, 530742520);
      X = D(X, W, V, Y, C[P + 2], j, 3299628645);
      Y = t(Y, X, W, V, C[P + 0], U, 4096336452);
      V = t(V, Y, X, W, C[P + 7], T, 1126891415);
      W = t(W, V, Y, X, C[P + 14], R, 2878612391);
      X = t(X, W, V, Y, C[P + 5], O, 4237533241);
      Y = t(Y, X, W, V, C[P + 12], U, 1700485571);
      V = t(V, Y, X, W, C[P + 3], T, 2399980690);
      W = t(W, V, Y, X, C[P + 10], R, 4293915773);
      X = t(X, W, V, Y, C[P + 1], O, 2240044497);
      Y = t(Y, X, W, V, C[P + 8], U, 1873313359);
      V = t(V, Y, X, W, C[P + 15], T, 4264355552);
      W = t(W, V, Y, X, C[P + 6], R, 2734768916);
      X = t(X, W, V, Y, C[P + 13], O, 1309151649);
      Y = t(Y, X, W, V, C[P + 4], U, 4149444226);
      V = t(V, Y, X, W, C[P + 11], T, 3174756917);
      W = t(W, V, Y, X, C[P + 2], R, 718787259);
      X = t(X, W, V, Y, C[P + 9], O, 3951481745);
      Y = K(Y, h);
      X = K(X, E);
      W = K(W, v);
      V = K(V, g)
    }
    var i = B(Y) + B(X) + B(W) + B(V);
    return i.toLowerCase()
  };

}
