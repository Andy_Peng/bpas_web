import {Component, OnInit} from '@angular/core';
import {BpAnalyticApiService} from '../bp-analytic-api-service/bp-analytic-api.service';
import {ChartOption} from "../vendor/OptionInterface/ChartOption"
import {Tools} from "../vendor/Tools/Tools"
import {GoJsDataModel} from '../vendor/OptionInterface/GoJsDataModel'
import {AppComponent} from "../app.component";
import * as _ from 'lodash'
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-service-experience-break-point-search',
  templateUrl: './service-experience-break-point-search.component.html',
  styleUrls: ['./service-experience-break-point-search.component.css'],
  providers: [BpAnalyticApiService]
})
export class ServiceExperienceBreakPointSearchComponent implements OnInit {

  flowChartOption: ChartOption = {width: 1000, height: 1200};
  protected flowChartDataSets: GoJsDataModel[] = [];
  protected selectedFlowChartDataSets: GoJsDataModel = {nodesModel: [], linksModel: []};
  protected flowChartHidden: boolean = true;
  protected clusters: any[] = [];
  protected selectedCluster: string = "";

  protected WeakScore: string[] = [];
  protected selectedWeakScore: string = "";


  protected projectList: any[] = [];
  protected selectedProject: string = "";

  protected casesList: any[] = [];
  protected selectedCasesList: any[] = [];
  protected selectedCase: string = "";


  protected TimeList: any[] = [];
  protected selectedTime: string = "";
  protected TimeLongList: number[] = [];


  protected startTime: Date = new Date(2003, 7, 10, 0, 0, 0, 0);
  protected endTime: Date = new Date(2003, 7, 10, 23, 59, 59, 99);

  constructor(private bpAnalyticApiService: BpAnalyticApiService) {
  }

  ngOnInit() {
    this.setProjectsData();

  }


  setProjectsData() {
    for (let i = 0; i < AppComponent.projects.length; i++) {
      this.projectList.push({
        label: AppComponent.projects[i].projectName,
        value: AppComponent.projects[i].projectName
      });
      this.casesList.push([]);
      for (let j = 0; j < AppComponent.projects[i].cases.length; j++) {
        this.casesList[i].push({
          label: AppComponent.projects[i].cases[j].caseName,
          value: AppComponent.projects[i].cases[j].caseName
        });
      }
    }

    this.selectedProject = this.projectList[0].label;
    this.selectedCasesList = this.casesList[0];
    this.selectedCase = this.selectedCasesList[0].label;

    // this.selectedProject = this.projectList[2].label;
    // this.selectedCasesList = this.casesList[2];
    // this.selectedCase = this.selectedCasesList[0].label;
    this.changeProjectAndCase();

  }

  changeProject_dropDownClick(event) {
    let i = _.findIndex(this.projectList, {label: event.value});
    this.selectedCasesList = this.casesList[i];
    this.selectedCase = this.selectedCasesList[0].label;
    this.changeProjectAndCase();

  }

  changeCase_dropDownClick(event) {
    let i = _.findIndex(this.selectedCasesList, {label: event.value});
    this.selectedCase = this.selectedCasesList[i].label;
    this.changeProjectAndCase();

    //console.log(this.selectedCase);


  }

  changeProjectAndCase() {
    //let response = [1060822171430, 1060503094118];
    let Filter = {
      projectName: this.selectedProject,
      caseName: this.selectedCase,
      property: 'recordTime'
    };


    this.bpAnalyticApiService.getHttpRequest(
      BpAnalyticApiService.ApiAis,
      BpAnalyticApiService.ApiAisRouteMapping.getProjectLongTimeArray,
      Filter).subscribe((response) => {

      this.TimeLongList = [];
      this.TimeList = [];

      //console.log(response);
      let tmpTimeStr = "";
      for (let k = 0; k < response.length; k++) {
        this.TimeLongList.push(response[k]['recordTime']);
        tmpTimeStr = Tools.formatDateYmdHms(new Date(response[k]['recordTime']), "-");
        this.TimeList.push({label: tmpTimeStr, value: tmpTimeStr});
      }
      this.selectedTime = this.TimeList[0].label;
    });
  }


  getFlowChartData() {

    let index = _.findIndex(this.TimeList, {label: this.selectedTime});

    //console.log(this.TimeLongList[index]);

    let Filter_ClusterRecord = {
      projectName: this.selectedProject,
      caseName: this.selectedCase,
      recordTime: this.TimeLongList[index]
    };

    let Filter_WeakSpot = {
      projectName: this.selectedProject,
      caseName: this.selectedCase,
      recordTime: this.TimeLongList[index],
      limit: 0.1,
      threshold: 'Average'
    };

    let promises = [];
    promises.push(this.bpAnalyticApiService.getHttpRequest(
      BpAnalyticApiService.ApiAis,
      BpAnalyticApiService.ApiAisRouteMapping['getWeakSpotByProject_CaseId'],
      Filter_WeakSpot));

    promises.push(this.bpAnalyticApiService.getHttpRequest(
      BpAnalyticApiService.ApiAis,
      BpAnalyticApiService.ApiAisRouteMapping['getClusterRecordByProjectId_CaseId'],
      Filter_ClusterRecord));

    Observable.forkJoin(promises)
      .subscribe((response) => {
        // response[0] ===>斷點資料
        // response[1] ===>群集資料


        let scoreObjKeys = Object.keys(response[0]['score']);


        this.clusters = [];
        this.WeakScore = [];

        for (let i = 0; i < parseInt(response[1]['numOfCluster']); i++) {
          this.clusters.push({
            label: '群集' + (i + 1),
            value: '群集' + (i + 1)
          });
          this.WeakScore.push(response[0]['score'][scoreObjKeys[i]]);
        }
        this.selectedCluster = this.clusters[0].label;

        console.log("debug!!!!--------------");

        let g = Tools.getWeakSpotsArray(response[0]['weakSpot']);
        this.flowChartDataSets = Tools.clusterProcessRecordDataToGoJsModel(response[1], g);
        this.changePlotOfCluster(0);

        console.log("debug!!!!--------------");
      });
  }

  changePlotOfCluster_dropDownClick(event) {
    let index = event.value.substr(2, event.value.length - 1);
    this.changePlotOfCluster(parseInt(index) - 1);

  }

  changePlotOfCluster(index) {
    this.selectedWeakScore = this.WeakScore[index];
    this.selectedFlowChartDataSets = this.flowChartDataSets[index];
    this.flowChartHidden = false;

    //this.selectedCaseData = this.selectedCaseDataArray[index];

  }


}
