import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceExperienceBreakPointSearchComponent } from './service-experience-break-point-search.component';

describe('ServiceExperienceBreakPointSearchComponent', () => {
  let component: ServiceExperienceBreakPointSearchComponent;
  let fixture: ComponentFixture<ServiceExperienceBreakPointSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceExperienceBreakPointSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceExperienceBreakPointSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
