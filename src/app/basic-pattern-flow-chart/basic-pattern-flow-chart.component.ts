import {Component, ElementRef, Input, AfterViewInit, ViewChild, OnInit} from '@angular/core';
import {ChartOption} from '../vendor/OptionInterface/ChartOption'
import {GoJsDataModel} from '../vendor/OptionInterface/GoJsDataModel'

import * as go from "gojs";

@Component({
  selector: 'basic-pattern-flow-chart',
  templateUrl: './basic-pattern-flow-chart.component.html',
  styleUrls: ['./basic-pattern-flow-chart.component.css']
})
export class BasicPatternFlowChartComponent implements OnInit {


  @ViewChild('myDiagramDiv')
  private element: ElementRef;
  private diagram: go.Diagram;
  private afterViewInitFlag: boolean = false;
  private $ = go.GraphObject.make;

  _option: ChartOption = null;

  @Input()
  set Option(option) {
    if (option === undefined) {
      return;
    }
    this._option = option;

  }

  get Option(): ChartOption {
    return this._option;
  }


  _data: GoJsDataModel = null;
  @Input()
  set data(data) {
    if (this._option === null) {
      return;
    }
    if (data === undefined) {
      return;
    }

    this._data = data;
    this.changeGoModelDataSet();


  }

  get data(): GoJsDataModel {
    return this._data;

  }

  changeGoModelDataSet() {

    if (this.afterViewInitFlag) {
      this.diagram.model = this.$(
        go.GraphLinksModel,
        {
          // linkFromPortIdProperty: "fromPort",
          // linkToPortIdProperty: "toPort",
          nodeDataArray: this._data.nodesModel,
          linkDataArray: this._data.linksModel,
        }
      );
    }


  }


  constructor() {
  }

  ngOnInit(): void {

  }

  ngAfterViewInit() {
    this.diagram = this.$(go.Diagram, this.element.nativeElement,
      {
        initialContentAlignment: go.Spot.Center,  // center the content
        "undoManager.isEnabled": true  // enable undo & redo
      });

    //console.log(this._data);

    this.diagram.nodeTemplate =
      this.$(go.Node, "Auto",  // the Shape will go around the TextBlock
        this.$(go.Shape, "RoundedRectangle", {strokeWidth: 0, fill: "lightblue"},
          // Shape.fill is bound to Node.data.color
          //new go.Binding("fill", "color")
        ),
        this.$(go.TextBlock,
          {margin: 8},  // some room around the text
          new go.Binding("text", "name"))
      );

    this.diagram.linkTemplate =
      this.$(go.Link,
        {
          routing: go.Link.AvoidsNodes,
          corner: 10,
          // curve: go.Link.JumpOver
        },
        this.$(go.Shape),                           // this is the link shape (the line)
        this.$(go.Shape, {toArrow: "Standard"}),    // this is an arrowhead

        //this.$(go.TextBlock, {stroke: 'blue', segmentIndex: 0, segmentFraction: 0.3}, new go.Binding("text", "rate")),
        this.$(go.TextBlock, {stroke: 'blue'}, new go.Binding("text", "rate"))
      );

    this.diagram.layout = this.$(go.TreeLayout, {comparer: go.LayoutVertex.smartComparer});

    this.afterViewInitFlag = true;

    if (this._option !== null) {
      if (this._data !== undefined) {
        this.changeGoModelDataSet();
      }
    }


  }

}
