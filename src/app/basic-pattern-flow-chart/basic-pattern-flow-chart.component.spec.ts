import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicPatternFlowChartComponent } from './basic-pattern-flow-chart.component';

describe('BasicPatternFlowChartComponent', () => {
  let component: BasicPatternFlowChartComponent;
  let fixture: ComponentFixture<BasicPatternFlowChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicPatternFlowChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicPatternFlowChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
