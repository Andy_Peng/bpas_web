import {Component, OnInit} from '@angular/core';
import {BpAnalyticApiService} from '../bp-analytic-api-service/bp-analytic-api.service';
import {UserAuthService} from '../user-auth-service/user-auth.service';
import {ChartOption} from "../vendor/OptionInterface/ChartOption"
import {Tools} from "../vendor/Tools/Tools"
import {GoJsDataModel} from '../vendor/OptionInterface/GoJsDataModel'
import {projectUploadTemplate} from "../vendor/OptionInterface/projectUploadTemplate";
import {AppComponent} from "../app.component";
import * as _ from 'lodash'
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-break-point-pattern-management',
  templateUrl: './break-point-pattern-management.component.html',
  styleUrls: ['./break-point-pattern-management.component.css'],
  providers: [BpAnalyticApiService, UserAuthService]

})
export class BreakPointPatternManagementComponent implements OnInit {
  private innerHeight: any;
  private innerWidth: any;


  protected startTime: Date = new Date(2003, 7, 10, 0, 0, 0, 0);
  protected endTime: Date = new Date(2003, 7, 10, 23, 59, 59, 999);


  // protected PatternsChartOptions: ChartOption[] = [];
  // protected flowChartDataSets: GoJsDataModel[] = [];

  protected PatternsChartInfo: any[] = [];
  protected projects: projectUploadTemplate[] = [];
  protected showPatternDialog: boolean = false;
  //protected firstPass: boolean = false;
  protected newPatternReason: string = "";
  protected selectedPatternReasonIndex: number;

  protected selectedPatternInfo: any = {
    goJsDataModel: {
      nodesModel: [],
      linksModel: []
    }
  };

  protected selectedPatternOption: any = {width: 600, height: 80};

  protected changeReasonHidden: boolean = true;
  protected editableReason: string = "";


  constructor(private bpAnalyticApiService: BpAnalyticApiService, private userAuthService: UserAuthService) {

  }

  genNewPatternReason() {

    this.bpAnalyticApiService.postHttpRequest(BpAnalyticApiService.ApiAis,
      BpAnalyticApiService.ApiAisRouteMapping.createNewPatternReason,
      {reason: this.newPatternReason, patternId: this.selectedPatternInfo.id})
      .subscribe((response) => {
        //console.log(response);
        this.selectedPatternInfo["patternReasons"].push(response);
        this.selectedPatternInfo["patternReasons"] = JSON.parse(JSON.stringify(this.selectedPatternInfo["patternReasons"]));
        this.newPatternReason = "";
      })

  }

  ngOnInit() {
    this.projects = AppComponent.projects;
    //this.getAllPatternsData();
  }

  getAllPatternsData() {
    let promises = [];

    for (let i = 0; i < this.projects.length; i++) {
      for (let j = 0; j < this.projects[i].cases.length; j++) {
        //this.projects[i].projectName
        //this.projects[i].cases[j].caseName
        let Filter = {
          projectName: this.projects[i].projectName,
          caseName: this.projects[i].cases[j].caseName,
          startDate: Tools.formatDateYmdHms(this.startTime, "-"),
          endDate: Tools.formatDateYmdHms(this.endTime, "-"),
          direction: 'ASC',
          properties: 'id'
        };
        promises.push(this.bpAnalyticApiService.getHttpRequest(
          BpAnalyticApiService.ApiAis,
          BpAnalyticApiService.ApiAisRouteMapping['getPatternsByProjectId_CaseId'],
          Filter));
      }
    }

    Observable.forkJoin(promises)
      .subscribe((response) => {
        let rlt = [];
        for (let x = 0; x < response.length; x++) {
          for (let i = 0; i < response[x]['content'].length; i++) {
            let goJsDataModelTmp = JSON.parse(response[x]['content'][i].relationshipChi);
            for (let j = 0; j < goJsDataModelTmp.links.length; j++) {
              goJsDataModelTmp.links[j].rate = (goJsDataModelTmp.links[j].rate * 100).toFixed(0) + "%";
            }
            response[x]['content'][i]['goJsDataModel'] = {
              nodesModel: goJsDataModelTmp.nodes,
              linksModel: goJsDataModelTmp.links
            };
            response[x]['content'][i]['goJsOption'] = {
              width: 600, height: 80
            };
            rlt.push(response[x]['content'][i]);
          }
        }
        //console.log(rlt);

        this.PatternsChartInfo = rlt;
        //console.log(this.PatternsChartInfo);
        //this.changeSelectedPatternData(0);


      }, (error) => {
        console.log(error);
      });
  }

  changeSelectedPatternData(index) {
    // if (index == 0) {
    //   this.selectedPatternInfo = this.PatternsChartInfo[2];
    //
    // } else {
    this.selectedPatternInfo = this.PatternsChartInfo[index];
    //}

    //if (this.firstPass) {
    this.showPatternDialog = true;
    //}
    //this.firstPass = true;
  }


  deleteSelectedPatternData(index) {
    console.log(this.PatternsChartInfo[index]);

    this.bpAnalyticApiService.deleteHttpRequest(BpAnalyticApiService.ApiAis,
      BpAnalyticApiService.ApiAisRouteMapping.deletePattern,
      {id: this.PatternsChartInfo[index].id})
      .subscribe((response) => {
        console.log(response);
        this.PatternsChartInfo.splice(index, 1);
        this.PatternsChartInfo = JSON.parse(JSON.stringify(this.PatternsChartInfo));
      })
  }

  changeSelectedPatternReason(i) {
    this.changeReasonHidden = false;
    //console.log(this.selectedPatternInfo["patternReasons"][i].reason);
    this.editableReason = JSON.parse(JSON.stringify(this.selectedPatternInfo["patternReasons"][i].reason))
    //this.editableReason = this.selectedPatternInfo["pattern"]

    this.selectedPatternReasonIndex = i;


  }

  deleteSelectedPatternReason(i) {

    console.log(this.selectedPatternInfo["patternReasons"][i].id);

    this.bpAnalyticApiService.deleteHttpRequest(BpAnalyticApiService.ApiAis,
      BpAnalyticApiService.ApiAisRouteMapping.createNewPatternReason,
      {id: this.selectedPatternInfo["patternReasons"][i].id})
      .subscribe((response) => {
        console.log(response);

        this.selectedPatternInfo["patternReasons"].splice(i, 1);
        this.selectedPatternInfo["patternReasons"] = JSON.parse(JSON.stringify(this.selectedPatternInfo["patternReasons"]));
      })

  }

  updateReason() {
    //console.log(this.selectedPatternInfo["patternReasons"][this.selectedPatternReasonIndex]);
    this.selectedPatternInfo["patternReasons"][this.selectedPatternReasonIndex].reason = JSON.parse(JSON.stringify(this.editableReason));
    let putPattern = this.selectedPatternInfo["patternReasons"][this.selectedPatternReasonIndex];

    console.log(this.selectedPatternInfo.id);
    console.log(putPattern);

    this.bpAnalyticApiService.putHttpRequest(BpAnalyticApiService.ApiAis,
      BpAnalyticApiService.ApiAisRouteMapping.createNewPatternReason,
      putPattern.id,
      {
        "id": putPattern.id,
        "patternId": this.selectedPatternInfo.id,
        "reason": putPattern.reason
      })
      .subscribe((response) => {
        console.log(response);
        this.changeReasonHidden = true;
        this.editableReason = "";
      })

  }

  hideDialog() {
    this.changeReasonHidden = true;

  }
}
