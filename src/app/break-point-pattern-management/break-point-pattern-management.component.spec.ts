import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreakPointPatternManagementComponent } from './break-point-pattern-management.component';

describe('BreakPointPatternManagementComponent', () => {
  let component: BreakPointPatternManagementComponent;
  let fixture: ComponentFixture<BreakPointPatternManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BreakPointPatternManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreakPointPatternManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
