import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicFlowChartComponent } from './basic-flow-chart.component';

describe('BasicFlowChartComponent', () => {
  let component: BasicFlowChartComponent;
  let fixture: ComponentFixture<BasicFlowChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicFlowChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicFlowChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
