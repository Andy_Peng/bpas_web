import {Component, ElementRef, Input, AfterViewInit, ViewChild, OnInit} from '@angular/core';
import {ChartOption} from '../vendor/OptionInterface/ChartOption'
import {GoJsDataModel} from '../vendor/OptionInterface/GoJsDataModel'


import * as go from "gojs";

@Component({
  selector: 'case-flow-chart',
  templateUrl: './case-flow-chart.component.html',
  styleUrls: ['./case-flow-chart.component.css']
})
export class CaseFlowChartComponent implements OnInit, AfterViewInit {
  @ViewChild('myDiagramDiv')
  private element: ElementRef;
  private diagram: go.Diagram;
  private afterViewInitFlag: boolean = false;
  private $ = go.GraphObject.make;


  _option: ChartOption = null;

  @Input()
  set Option(option) {
    if (option === undefined) {
      return;
    }
    this._option = option;
    console.log(this._option);
  }

  get Option(): ChartOption {
    return this._option;
  }


  _data: GoJsDataModel = null;

  @Input()
  set data(data) {
    if (this._option === null) {
      return;
    }
    if (data === undefined) {
      return;
    }

    this._data = data;
    this.changeGoModelDataSet();


  }

  get data(): GoJsDataModel {
    return this._data;

  }


  changeGoModelDataSet() {

    if (this.afterViewInitFlag) {
      this.diagram.model = this.$(
        go.GraphLinksModel,
        {
          linkFromPortIdProperty: "fromPort",
          linkToPortIdProperty: "toPort",
          nodeDataArray: this._data.nodesModel,
          linkDataArray: this._data.linksModel,
        }
      );
    }
  }

  constructor() {
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.diagram = this.$(go.Diagram, this.element.nativeElement,
      {
        initialContentAlignment: go.Spot.Center,  // center the content
        "undoManager.isEnabled": true  // enable undo & redo
      });

    // define a simple Node template
    // this.diagram.nodeTemplate =
    //   this.$(go.Node, "Auto",
    //     new go.Binding("location", "loc", go.Point.parse),
    //     this.$(go.Shape, "RoundedRectangle", new go.Binding("fill", "color")),
    //     this.$(go.TextBlock, {margin: 5, isMultiline: true}, new go.Binding("text", "text"))
    //   );

    let portSize = new go.Size(8, 8);

    this.diagram.nodeTemplate =
      this.$(go.Node, "Table",
        {
          locationObjectName: "BODY",
          locationSpot: go.Spot.Center,
          selectionObjectName: "BODY",
        },

        new go.Binding("location", "loc", go.Point.parse),
        // the body
        this.$(go.Panel, "Auto",
          {
            row: 1, column: 1, name: "BODY",
            stretch: go.GraphObject.Fill
          },
          this.$(go.Shape, "Rectangle",
            {
              stroke: null, strokeWidth: 0,
              minSize: new go.Size(60, 80)
            }, new go.Binding("fill", "color")),
          this.$(go.TextBlock,
            {margin: 10, textAlign: "center", font: "14px  Segoe UI,sans-serif", stroke: "black", editable: true},
            //new go.Binding("text", "text"))
            new go.Binding("text", "name"))
        ),  // end Auto Panel body


        this.$(go.Panel, "Vertical",
          new go.Binding("itemArray", "leftArray"),
          {
            row: 1, column: 0,
            itemTemplate:
              this.$(go.Panel,
                {
                  _side: "left",  // internal property to make it easier to tell which side it's on
                  fromSpot: go.Spot.Left, toSpot: go.Spot.Left,
                  fromLinkable: true, toLinkable: true, cursor: "pointer",
                },
                new go.Binding("portId", "portId"),
                this.$(go.Shape, "Rectangle",
                  {
                    stroke: null, strokeWidth: 0,
                    desiredSize: portSize,
                    margin: new go.Margin(1, 0)
                  },
                  new go.Binding("fill", "portColor"))
              )  // end itemTemplate
          }
        ),  // end Vertical Panel


        // the Panel holding the right port elements, which are themselves Panels,
        // created for each item in the itemArray, bound to data.rightArray
        this.$(go.Panel, "Vertical",
          new go.Binding("itemArray", "rightArray"),
          {
            row: 1, column: 2,
            itemTemplate:
              this.$(go.Panel,
                {
                  _side: "right",
                  fromSpot: go.Spot.Right, toSpot: go.Spot.Right,
                  fromLinkable: true, toLinkable: true, cursor: "pointer",
                  opacity: 0.7
                },
                new go.Binding("portId", "portId"),
                this.$(go.Shape, "Rectangle",
                  {
                    stroke: null, strokeWidth: 0,
                    desiredSize: portSize,
                    margin: new go.Margin(1, 0)
                  },
                  new go.Binding("fill", "portColor"))
              )  // end itemTemplate
          }
        ),  // end Vertical Panel

      );


    this.diagram.linkTemplate =
      this.$(go.Link,
        {
          routing: go.Link.AvoidsNodes,
          corner: 10,
          // curve: go.Link.JumpOver
        },
        this.$(go.Shape),                           // this is the link shape (the line)
        this.$(go.Shape, {toArrow: "Standard"}),    // this is an arrowhead
        //this.$(go.TextBlock, {stroke: 'blue'}, new go.Binding("text", "text"))
        this.$(go.TextBlock, {stroke: 'blue', segmentIndex: 0, segmentFraction: 0.3}, new go.Binding("text", "rate"))
      );

    //this.diagram.layout = new go.CircularLayout();
    //this.diagram.layout = $(go.TreeLayout, {comparer: go.LayoutVertex.smartComparer});

    this.afterViewInitFlag = true;

    if (this._option !== null) {
      if (this._data !== undefined) {
        this.changeGoModelDataSet();
      }
    }

  }


  // makePort(name, spot, output, input) {
  //   // the port is basically just a small circle that has a white stroke when it is made visible
  //   return this.$(go.Shape, "rectangle",
  //     {
  //       //fill: "transparent",
  //       fill: "black",
  //       stroke: "red",  // this is changed to "white" in the showPorts function
  //       desiredSize: new go.Size(8, 3),
  //       alignment: spot, alignmentFocus: spot,  // align the port on the main Shape
  //       portId: name,  // declare this object to be a "port"
  //       fromSpot: spot, toSpot: spot,  // declare where links may connect at this port
  //       fromLinkable: output, toLinkable: input,  // declare whether the user may draw links to/from here
  //       cursor: "pointer"  // show a different cursor to indicate potential link point
  //     });
  // }

  // this.diagram.nodeTemplate.add(
  //   makePort("top", go.Spot.Top, true, true)
  // );
  // this.diagram.nodeTemplate.add(
  //   makePort("lalala", go.Spot.Top, true, true)
  // );
  //
  // this.diagram.nodeTemplate.add(
  //   makePort("left", go.Spot.Left, true, true)
  // );


}
