import {Injectable} from '@angular/core';
import {Http, Response, URLSearchParams} from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/toPromise.js';
import * as moment from 'moment/moment';


@Injectable()
export class UserAuthService {


  public static ApiAnalysis = {
    Ip: '220.135.250.147',
    Port: '28080',
    Domain: 'lohas-uiux-analysis'
  };


  public static ApiAnalysisRouteMapping = {
    login: '/doLogin',
    getUserProject: '/Index/queryUserProject2'
  };


  constructor(private http: Http) {
  }

  getHttpRequest(ApiInfo, RouteMappingInfo, Filter) {
    let url = 'http://' + ApiInfo.Ip +
      ':' + ApiInfo.Port +
      '/' + ApiInfo.Domain + RouteMappingInfo;

    let params = new URLSearchParams();

    let getParamKeys = Object.keys(Filter);
    for (let i = 0; i < getParamKeys.length; i++) {
      params.set(getParamKeys[i], Filter[getParamKeys[i]]);
    }

    return this.http
      .get(url, {'search': params})
      .map(this.extractDataBase)
      .catch(this.handleErrorPromise);
  }


  postHttpRequest(ApiInfo, RouteMappingInfo, Filter) {
    let url = 'http://' + ApiInfo.Ip +
      ':' + ApiInfo.Port +
      '/' + ApiInfo.Domain + RouteMappingInfo;

    let params = new URLSearchParams();

    let getParamKeys = Object.keys(Filter);
    for (let i = 0; i < getParamKeys.length; i++) {
      params.set(getParamKeys[i], Filter[getParamKeys[i]]);
    }

    // let headers = new Headers();
    // headers.append("content-type","application/json;charset=utf-8");

    return this.http
      .post(url, params)
      .map(this.extractDataBase)
      .catch(this.handleErrorPromise);
  }

  login(ApiInfo, RouteMappingInfo, Filter) {
    let url = 'http://' + ApiInfo.Ip +
      ':' + ApiInfo.Port +
      '/' + ApiInfo.Domain + RouteMappingInfo +
      "?account=" + Filter.account + "&password=" + Filter.password;


    return this.http.post(url, {})
      .map(this.extractDataBase)
      .catch(this.handleErrorPromise);

  }



  private extractDataBase(res: Response): any {
    let resObj = res.json();
    return resObj || [];
  }

  private handleErrorPromise(error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }


}
