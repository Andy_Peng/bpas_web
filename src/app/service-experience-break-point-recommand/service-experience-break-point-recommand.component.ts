import {Component, OnInit} from '@angular/core';
import {BpAnalyticApiService} from '../bp-analytic-api-service/bp-analytic-api.service';
import {ChartOption} from "../vendor/OptionInterface/ChartOption"
import {Tools} from "../vendor/Tools/Tools"
import {GoJsDataModel} from '../vendor/OptionInterface/GoJsDataModel'
import {AppComponent} from "../app.component";
import * as _ from 'lodash'
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-service-experience-break-point-recommand',
  templateUrl: './service-experience-break-point-recommand.component.html',
  styleUrls: ['./service-experience-break-point-recommand.component.css'],
  providers: [BpAnalyticApiService]
})
export class ServiceExperienceBreakPointRecommandComponent implements OnInit {
  private innerHeight: any;
  private innerWidth: any;


  protected startTime: Date = new Date(2003, 7, 10, 0, 0, 0, 0);
  protected endTime: Date = new Date(2003, 7, 10, 23, 59, 59, 999);

  protected flowChartOption: ChartOption = {width: 1000, height: 1200};
  protected flowChartDataSets: GoJsDataModel[] = [];
  protected selectedFlowChartDataSets: GoJsDataModel = {nodesModel: [], linksModel: []};
  protected flowChartHidden: boolean = true;
  protected clusters: any[] = [];
  protected selectedCluster: string = "";

  protected WeakScore: string = "";
  protected selectedWeakScore: string = "";


  protected projectList: any[] = [];
  protected selectedProject: string = "";

  protected casesList: any[] = [];
  protected selectedCasesList: any[] = [];
  protected selectedCase: string = "";


  // protected patternChartDataSets: GoJsDataModel[] = [];
  // protected patternChartOptions: ChartOption[] = [];


  protected selectedCaseDataSet: any = {content: []};
  protected selectedCaseDataArray: any[][] = [];
  protected selectedCaseData: any[] = [];


  //for Viewing pattern
  protected showRecommandDialog: boolean = false;

  protected ViewingPatternOption: ChartOption = {width: 400, height: 80};
  protected ViewingPattern: any = {
    goJsDataModel: {
      nodesModel: [],
      linksModel: []
    }
  };
  protected ViewingFlowChartOption: ChartOption = {width: 630, height: 450};
  protected ViewingFlowData: GoJsDataModel = {nodesModel: [], linksModel: []};


  constructor(private bpAnalyticApiService: BpAnalyticApiService) {
  }

  ngOnInit() {
    this.setProjectsData();
    this.changeProjectAndCase()
  }


  setProjectsData() {
    for (let i = 0; i < AppComponent.projects.length; i++) {
      this.projectList.push({
        label: AppComponent.projects[i].projectName,
        value: AppComponent.projects[i].projectName
      });
      this.casesList.push([]);
      for (let j = 0; j < AppComponent.projects[i].cases.length; j++) {
        this.casesList[i].push({
          label: AppComponent.projects[i].cases[j].caseName,
          value: AppComponent.projects[i].cases[j].caseName
        });
      }
    }

    // this.selectedProject = this.projectList[0].label;
    // this.selectedCasesList = this.casesList[0];
    // this.selectedCase = this.selectedCasesList[0].label;

    this.selectedProject = this.projectList[0].label;
    this.selectedCasesList = this.casesList[0];
    this.selectedCase = this.selectedCasesList[0].label;

  }


  changeProject_dropDownClick(event) {
    let i = _.findIndex(this.projectList, {label: event.value});
    this.selectedCasesList = this.casesList[i];
    this.selectedCase = this.selectedCasesList[0].label;
  }

  changeCase_dropDownClick(event) {
    let i = _.findIndex(this.selectedCasesList, {label: event.value});
    this.selectedCase = this.selectedCasesList[i].label;

    //console.log(this.casesList);
    console.log(this.selectedCase);

    //this.selectedCase = this.selectedCasesList[i].label;
  }

  changeProjectAndCase() {
    let Filter = {
      projectName: this.selectedProject,
      caseName: this.selectedCase,
      startDate: Tools.formatDateYmdHms(this.startTime, "-"),
      endDate: Tools.formatDateYmdHms(this.endTime, "-"),
      direction: 'ASC',
      properties: 'id'
    };

    // let answer = this.bpAnalyticApiService.getSEBPRAllData([Filter, {}]);
    // console.log(answer);


    this.bpAnalyticApiService.getHttpRequest(
      BpAnalyticApiService.ApiAis,
      BpAnalyticApiService.ApiAisRouteMapping['getPatternsByProjectId_CaseId'],
      Filter
    ).subscribe((response) => {

      this.selectedCaseDataSet = response;
      this.selectedCaseData = [];

      //clusterIndex
      let clusterIndexTmp = 0;

      for (let i = 0; i < this.selectedCaseDataSet.content.length; i++) {
        let goJsDataModelTmp = JSON.parse(this.selectedCaseDataSet.content[i].relationshipChi);
        for (let j = 0; j < goJsDataModelTmp.links.length; j++) {
          goJsDataModelTmp.links[j].rate = (goJsDataModelTmp.links[j].rate * 100).toFixed(0) + "%";
        }
        this.selectedCaseDataSet.content[i]['goJsDataModel'] = {
          nodesModel: goJsDataModelTmp.nodes,
          linksModel: goJsDataModelTmp.links
        };
        this.selectedCaseDataSet.content[i]['goJsOption'] = {
          width: 750, height: 130
        };


        //   clusterIndexTmp = parseInt(this.selectedCaseDataSet.content[i]['clusterIndex']);
        //   console.log(clusterIndexTmp);
        //
        //   if (typeof(this.selectedCaseDataArray[clusterIndexTmp]) === 'undefined') {
        //     this.selectedCaseDataArray[clusterIndexTmp] = [];
        //     this.selectedCaseDataArray[clusterIndexTmp].push(this.selectedCaseDataSet.content[i]);
        //   } else {
        //
        //     this.selectedCaseDataArray[clusterIndexTmp].push(this.selectedCaseDataSet.content[i]);
        //   }

        this.selectedCaseData.push(this.selectedCaseDataSet.content[i]);
      }

      this.selectedCaseData = JSON.parse(JSON.stringify(this.selectedCaseData));


    })
  }

  // changePlotOfCluster_dropDownClick(event) {
  //   let index = event.value.substr(2, event.value.length - 1);
  //   this.changePlotOfCluster(parseInt(index) - 1);
  // }

  // changePlotOfCluster(index) {
  //   this.selectedFlowChartDataSets = this.flowChartDataSets[index];
  //   this.selectedCaseData = this.selectedCaseDataArray[index];
  //   this.selectedWeakScore = this.WeakScores[index];
  //
  //
  //   this.flowChartHidden = false;
  //
  // }

  showRecommandPlots(data, i) {

    this.ViewingPattern = data;

    let d = new Date(this.selectedCaseDataSet.content[0]['recordDateTime']);
    let dateTimeLong = d.getTime();

    let clusterIndex = parseInt(this.selectedCaseDataSet.content[i]['clusterIndex']);
    console.log(dateTimeLong);


    let Filter_ClusterRecord = {
      projectName: this.selectedProject,
      caseName: this.selectedCase,
      recordTime: dateTimeLong
    };

    let Filter_WeakSpot = {
      projectName: this.selectedProject,
      caseName: this.selectedCase,
      recordTime: dateTimeLong,
      limit: 0.1,
      threshold: 'Average'
    };

    let promises = [];
    promises.push(this.bpAnalyticApiService.getHttpRequest(
      BpAnalyticApiService.ApiAis,
      BpAnalyticApiService.ApiAisRouteMapping['getWeakSpotByProject_CaseId'],
      Filter_WeakSpot));

    promises.push(this.bpAnalyticApiService.getHttpRequest(
      BpAnalyticApiService.ApiAis,
      BpAnalyticApiService.ApiAisRouteMapping['getClusterRecordByProjectId_CaseId'],
      Filter_ClusterRecord));

    Observable.forkJoin(promises)
      .subscribe((response) => {
        // response[0] ===>斷點資料
        // response[1] ===>群集資料

        console.log(response[0]);
        console.log(response[1]);

        let scoreObjKeys = Object.keys(response[0]['score']);

        console.log("clusterIndex:" + clusterIndex);

        this.WeakScore = response[0]['score'][scoreObjKeys[clusterIndex]].toFixed(5);

        console.log(this.WeakScore);
        // for (let i = 0; i < parseInt(response[1]['numOfCluster']); i++) {
        //   this.clusters.push({
        //     label: '群集' + (i + 1),
        //     value: '群集' + (i + 1)
        //   });
        // }
        // this.selectedCluster = this.clusters[0].label;

        let g = Tools.getWeakSpotsArray(response[0]['weakSpot']);

        if (!g.hasOwnProperty('' + clusterIndex)) {
          this.ViewingFlowData = Tools.ProcessCluster(response[1], clusterIndex, []);
        } else {
          this.ViewingFlowData = Tools.ProcessCluster(response[1], clusterIndex, g[clusterIndex]);
        }


        //this.flowChartDataSets = Tools.clusterProcessRecordDataToGoJsModel(response[1], g);
        //this.changePlotOfCluster(0);
      });


    //this.ViewingFlowData = this.selectedFlowChartDataSets;


    this.showRecommandDialog = true;

  }

}
