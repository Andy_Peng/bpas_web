import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceExperienceBreakPointRecommandComponent } from './service-experience-break-point-recommand.component';

describe('ServiceExperienceBreakPointRecommandComponent', () => {
  let component: ServiceExperienceBreakPointRecommandComponent;
  let fixture: ComponentFixture<ServiceExperienceBreakPointRecommandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceExperienceBreakPointRecommandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceExperienceBreakPointRecommandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
